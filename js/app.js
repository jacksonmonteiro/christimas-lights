//Botões de ligar e desligar
const ligar = document.querySelector('#ligar');
const desligar = document.querySelector('#desligar');
const alterar = document.querySelector('#alterar');

//Luzes
let red = document.querySelector('.red');
let orange = document.querySelector('.orange');
let yellow = document.querySelector('.yellow');
let green = document.querySelector('.green');
let blue = document.querySelector('.blue');
let indigo = document.querySelector('.indigo');
let violet = document.querySelector('.violet');

//clique
let clique = 0;

//Ao iniciar a página todas as luzes estarão paradas
window.onload = function() {
	red.style.animationDuration = '0s';
	orange.style.animationDuration = '0s';
	yellow.style.animationDuration = '0s';
	green.style.animationDuration = '0s';
	blue.style.animationDuration = '0s';
	indigo.style.animationDuration = '0s';
	violet.style.animationDuration = '0s';
}

//Fará com que as luzes liguem
ligar.onclick = function() {
	red.style.animationDuration = '1s';
	orange.style.animationDuration = '1s';
	yellow.style.animationDuration = '1s';
	green.style.animationDuration = '1s';
	blue.style.animationDuration = '1s';
	indigo.style.animationDuration = '1s';
	violet.style.animationDuration = '1s';
}

//Desligara as luzes, voltando-as para seu estado inicial
desligar.onclick = function() {
	red.style.animationDuration = '0s';
	orange.style.animationDuration = '0s';
	yellow.style.animationDuration = '0s';
	green.style.animationDuration = '0s';
	blue.style.animationDuration = '0s';
	indigo.style.animationDuration = '0s';
	violet.style.animationDuration = '0s';
}

//Altera o intervalo das luzes
alterar.onclick = function() {
	clique += 1;

	if (clique == 0) {
		red.style.animationDuration = '1s';
		orange.style.animationDuration = '1s';
		yellow.style.animationDuration = '1s';
		green.style.animationDuration = '1s';
		blue.style.animationDuration = '1s';
		indigo.style.animationDuration = '1s';
		violet.style.animationDuration = '1s';
	} else if (clique == 1) {
		red.style.animationDuration = '1s';
		orange.style.animationDuration = '2s';
		yellow.style.animationDuration = '1s';
		green.style.animationDuration = '2s';
		blue.style.animationDuration = '1s';
		indigo.style.animationDuration = '2s';
		violet.style.animationDuration = '1s';
	} else if (clique == 2) {
		red.style.animationDuration = '1s';
		orange.style.animationDuration = '2s';
		yellow.style.animationDuration = '3s';
		green.style.animationDuration = '1s';
		blue.style.animationDuration = '2s';
		indigo.style.animationDuration = '3s';
		violet.style.animationDuration = '1s';
	} else if (clique == 3) {
		red.style.animationDuration = '1s';
		orange.style.animationDuration = '2s';
		yellow.style.animationDuration = '3s';
		green.style.animationDuration = '4s';
		blue.style.animationDuration = '3s';
		indigo.style.animationDuration = '2s';
		violet.style.animationDuration = '1s';
	} else {
		clique = 0;

		red.style.animationDuration = '1s';
		orange.style.animationDuration = '1s';
		yellow.style.animationDuration = '1s';
		green.style.animationDuration = '1s';
		blue.style.animationDuration = '1s';
		indigo.style.animationDuration = '1s';
		violet.style.animationDuration = '1s';
	}
}